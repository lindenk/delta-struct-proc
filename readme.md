Deltastruct-proc
===========

A Rust library to define and serialize custom changes to a type. 

Features an attribute macro to generate a delta type and implementation for any (most) of your own structs. This is the proc-macro backend for Deltastruct.